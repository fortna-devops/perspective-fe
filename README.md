# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repo holds backups of Ignition Perspective gateway settings
(*.gwbk files) and projects (*.zip files).  

### How do I get set up? ###
Gateway backup files should be "restored" on the Gateway config page of Perspective,
and project backup files should be "imported" on the Perspective Designer tool.

